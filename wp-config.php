<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'woshop' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         ':f@i.TOqzd:ywESC>|^|Hj:VVD(rG2d|yWv*&=M<:TTOK(5D{[Swuc3!6A+#iguM' );
define( 'SECURE_AUTH_KEY',  '|Y*Z%9f(LP^*|@ck.&:C?yo+cvYiM@Z%q=f<2d$wsuxCo6K85)$/Ae}OkdS}w-lb' );
define( 'LOGGED_IN_KEY',    'u8Ae(?eLI6hadjtN_=[`Aws4g6]rvq?)TiNkkLD1#8(P57Y1:Vqp[kbWuS [QPE<' );
define( 'NONCE_KEY',        'Ub?$>0q Yd5xA[0(m+pf$L;l+Vs|S5$*tnkn;>U#C4h- ;q]h}mjN:j56*0zU_9>' );
define( 'AUTH_SALT',        'T*`-fGZG_+7w>xYL2cFfRa{zEoY7-y,0~WzqgAcC!mP@R;+kg$)&AAxs]t%G@V O' );
define( 'SECURE_AUTH_SALT', 'g$uEUBpiVqtU]7R9yw9NIA1eA6=9Kw#&P%JxcNSxa1eR3wrdZ8D!eOBrUI16{U<w' );
define( 'LOGGED_IN_SALT',   'FsfcG $w:tfGD1n`zEbv}N:,6$X)82*N>vo(SG0Z3x%df}|9JK@En!.X>6ka64%,' );
define( 'NONCE_SALT',       'A bw y#W{cESd0EnxR~l3^~r. 3F-MVLKZX_[}nFGbXsV?<$ {QQ3r2/gNZC2R+k' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
