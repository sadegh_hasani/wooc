<?php

namespace App\MetaBoxes;

use App\Services\View;

class Product
{
    /*
     * TODO: get location options meta
     * TODO: handle class get location
     */
    const PRODUCT_LOCATION = 'product_location';

    const NEW_PRODUCT = 'new_product';

    const SPECIAL_PRODUCT = 'special_product';

    const SELLER = 'seller';

    const SPECIAL_OFFER = 'special_offer';

    const POPULAR_PRODUCTS = 'popular_products';


    public function register_product_metabox()
    {
        add_meta_box('product_location', 'جایگاه محصول', [$this, 'product_metabox_haandler'], 'product', 'side');
    }

    public function product_metabox_haandler($post)
    {

        $locations = $this->get_product_locations();

        $current_locations = (array)get_post_meta($post->ID, self::PRODUCT_LOCATION, true);

        $data = [
            'product_locations' => $locations,
            'current_locations' => $current_locations
        ];

        View::load('admin.product.metaboxes.product-location', $data);
    }

    public function save_product_location($post_id)
    {
        if (!$post_id) {
            return false;
        }

        if (isset($_POST['product_location']) && count($_POST['product_location'] > 0)) {
            update_post_meta($post_id, self::PRODUCT_LOCATION, $_POST['product_location']);
        } else {
            delete_post_meta($post_id, self::PRODUCT_LOCATION);
        }
    }

    public function get_product_locations()
    {
        /*
         * TODO get location by options meta
         * */
        $locations = [
            'محصول جدید' => 'new_product',
            'پرفروش' => 'seller',
            'محصول ویژه' => 'special_product',
            'پیشنهاد ویژه' => 'special_offer',
            'محصولات پربازدید' => 'Popular_products'
        ];

        return $locations;
    }

    public static function get_meta_product($meta_value)
    {
        $posts = get_posts(
            array(
                'post_type' => 'product',
                'posts_per_page' => -1,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_status' => 'publish',
                'meta_query' => array(
                    array(
                        'key' => 'product_location',
                        'value' => serialize(strval($meta_value)),
                        'compare' => 'LIKE',
                    ),
                ),
            )
        );

        return $posts;
    }
}
