<?php

namespace App\Utilities;

use App\Services\View;

class FlashMessage
{
    const SUCCESS = 1;

    const ERROR = 2;

    const WARNING = 3;

    const INFO = 4;

    public static $has_error = false;

    public static function add($msg, $type = self::SUCCESS)
    {
        if (!isset($_SESSION['flash_message'])) {
            $_SESSION['flash_message'] = [];
        }

        $_SESSION['flash_message'][] = (object)['msg' => $msg, 'type' => $type];


        if ($type == self::ERROR) {
            self::$has_error = true;
        }

        self::show_message();
    }

    public static function clear()
    {
        $_SESSION['flash_message'] = array();

        self::$has_error = false;
    }

    public static function get_message()
    {
        return $_SESSION['flash_message'] ?? [];
    }

    public static function show_message()
    {
        $msg = self::get_message();

        if (empty($msg)) {
            return;
        }

        $data = ['messages' => $msg];

        View::load('admin.notice.flash-message', $data);

        self::clear();
    }

    public static function get_css_type($type)
    {
        switch ($type) {
            case self::SUCCESS:
                return 'success';
                break;

            case self::ERROR:
                return 'error';
                break;

            case self::INFO:
                return 'info';
                break;

            case self::WARNING:
                return "warning";
                break;
        }
    }
}
