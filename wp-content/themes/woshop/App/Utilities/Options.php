<?php

namespace App\Utilities;

use App\OptionsPanel\optionsPanel;

class Options
{
    const WEBSITE_LOGO = 'website_logo';

    const YOUTUBE_LOGO = 'youtube_logo';

    const TWITTER_LOGO = 'twitter_logo';

    const INSTAGRAM_LOGO = 'instagram_logo';

    const TELEGRAM_LOGO = 'telegram_logo';

    const TWITTER_ADDRESS = 'twitter_address';

    const INSTAGRAM_ADDRESS = 'instagram_address';

    const TELEGRAM_ADDRESS = 'telegram_address';

    const YOUTUBE_ADDRESS = 'youtube_address';

    const SHOW_TELEGRAM = 'show_telegram';

    const SHOW_YOUTUBE = 'show_youtube';

    const SHOW_INSTAGRAM = 'show_instagram';

    const SHOW_TWITTER = 'show_twitter';



    public static function get_logo($logo_name, $get_name = false)
    {
        $options = get_option(optionsPanel::THEME_OPTION, []);

        if ($get_name === true) {
            return $options['global'][$logo_name];
            exit;
        }

        if ($options['global'][$logo_name]) {
            return LOGO_URL . $options['global'][$logo_name];
        }
    }


    public static function get_address($address_url)
    {
        $options = get_option(optionsPanel::THEME_OPTION, []);
        return isset($options['global'][$address_url]) ? $options['global'][$address_url] : "آدرسی یافت نشد";
    }

    public static function get_number_contact()
    {
        $options = get_option(optionsPanel::THEME_OPTION, []);
        return isset($options['global']['number_contact']) ? ($options['global']['number_contact']) : "شماره تماسی یافت نشد";
    }

    public static function is_show_logo($logo_name)
    {
        $options = get_option(optionsPanel::THEME_OPTION, []);

        if ($options['global'][$logo_name]) {
            return true;
        } else {
            return false;
        }
    }
}
