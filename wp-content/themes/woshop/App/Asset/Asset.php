<?php

namespace App\Asset;


class Asset
{

    private $asset_theme;

    public function __construct()
    {
        $this->asset_theme = THEME_URL . 'assets/';
    }

    public function register_assets_theme()
    {
        $this->enqueue_style();
        $this->enqueue_scripts();
    }

    public function enqueue_style()
    {
        /*  Icon Font CSS */
        wp_enqueue_style('all.min.css', $this->asset_theme . 'css/all.min.css');
        wp_enqueue_style('ionicons.min.css', $this->asset_theme . 'css/ionicons.min.css');
        wp_enqueue_style('themify-icons.css', $this->asset_theme . 'css/themify-icons.css');
        wp_enqueue_style('css/linearicons.css', $this->asset_theme . 'css/linearicons.css');
        wp_enqueue_style('css/flaticon.css', $this->asset_theme . 'css/flaticon.css');
        wp_enqueue_style('css/css/simple-line-icons.css', $this->asset_theme . 'css/css/simple-line-icons.css');


        /* owl carousel CSS */
        wp_enqueue_style('', $this->asset_theme . 'owlcarousel/css/owl.carousel.min.css');
        wp_enqueue_style('', $this->asset_theme . 'owlcarousel/css/owl.theme.css');
        wp_enqueue_style('', $this->asset_theme . 'owlcarousel/css/owl.theme.default.min.css');

        /* Magnific Popup CSS */
        wp_enqueue_style('magnific-popup.css', $this->asset_theme . 'css/magnific-popup.css');

        /* Slick CSS */
        wp_enqueue_style('slick.css', $this->asset_theme . 'css/slick.css');
        wp_enqueue_style('slick-theme.css', $this->asset_theme . 'css/slick-theme.css');

        /*Style CSS*/
        wp_enqueue_style('style.css', $this->asset_theme . 'css/style.css');
        wp_enqueue_style('responsive.css', $this->asset_theme . 'css/responsive.css');

        /* RTL CSS */
        wp_enqueue_style('rtl-style.css', $this->asset_theme . 'css/rtl-style.css');

        /* customize front */
        wp_enqueue_style('rtl-style.css', $this->asset_theme . 'css/css/customize-front.css');

    }

    public function enqueue_scripts()
    {
        /* popper min js */
        wp_enqueue_script('popper.min', $this->asset_theme . 'js/popper.min.js', ['jquery'], true, true);

        /* Latest compiled and minified Bootstrap */
        wp_enqueue_script('bootstrap.min', $this->asset_theme . 'bootstrap/js/bootstrap.min.js', ['jquery'], true, true);

        /*  owl-carousel min js */
        wp_enqueue_script('owl.carousel.min', $this->asset_theme . 'owlcarousel/js/owl.carousel.min.js', ['jquery'], true, true);

        /* magnific-popup min js */
        wp_enqueue_script('magnific-popup.min', $this->asset_theme . 'js/magnific-popup.min.js', ['jquery'], true, true);

        /*  waypoints min js */
        wp_enqueue_script('waypoints.min', $this->asset_theme . 'js/waypoints.min.js', ['jquery'], true, true);

        /* parallax js */
        wp_enqueue_script('parallax', $this->asset_theme . 'js/parallax.js', ['jquery'], true, true);

        /*  countdown js  */
        wp_enqueue_script('jquery.countdown.min', $this->asset_theme . 'js/jquery.countdown.min.js', ['jquery'], true, true);

        /*  imagesloaded js  */
        wp_enqueue_script('imagesloaded.pkgd.min', $this->asset_theme . 'js/imagesloaded.pkgd.min.js', ['jquery'], true, true);

        /* isotope min js */
        wp_enqueue_script('isotope.min', $this->asset_theme . 'js/isotope.min.js', ['jquery'], true, true);

        /*  jquery.dd.min js */
        wp_enqueue_script('jquery.dd.min', $this->asset_theme . 'js/jquery.dd.min.js', ['jquery'], true, true);

        /* slick js  */
        wp_enqueue_script('slick.min', $this->asset_theme . 'js/slick.min.js', ['jquery'], true, true);

        /* elevatezoom js */
        wp_enqueue_script('jquery.elevatezoom', $this->asset_theme . 'js/jquery.elevatezoom.js', ['jquery'], true, true);

        /* scripts js */
        wp_enqueue_script('scripts', $this->asset_theme . 'js/scripts.js', ['jquery'], true, true);
    }
}