<?php

namespace App\OptionsPanel;

use App\Services\Uploader;
use App\Services\View;
use App\Utilities\Options;

class optionsPanel
{
    protected $layout;
    const THEME_OPTION = 'theme_option';

    public function __construct()
    {
        $this->layout = 'admin.options-panel';
        add_action('save_option_theme', [$this, 'save_options']);
    }

    public function register_options_panel_menu()
    {
        add_theme_page('تنظیمات قالب', 'تنظیمات قالب', 'manage_options', 'options_panel', [$this, 'options_panel_handler']);
    }

    public function options_panel_handler()
    {

        $tabs = ($_GET['tab']) ?? 'global';
        if (!in_array($tabs, $this->white_list_page())) {
            die('دسترسی به این بخش امکام پذیر نیست');
        }


        if (isset($_POST['otpion_theme_save'])) {
            do_action('save_option_theme', $tabs);
        }

        $options = get_option(self::THEME_OPTION, []);
        $theme_options = $options[$tabs] ?? [];
        $data = ['tabs' => $tabs, 'options' => $theme_options];
        View::load("admin.options-panel.{$tabs}", $data, $this->layout);
    }


    private function white_list_page()
    {
        $pages = ['global', 'post'];
        return $pages;
    }


    public function save_options($tabs)
    {

        $options = get_option('', []);

        $weblogo = new Uploader('website_logo', 'logo');

        $twilogo = new Uploader('twitter_logo', 'logo');

        $inslogo = new Uploader('instagram_logo', 'logo');

        $tellogo = new Uploader('telegram_logo', 'logo');

        $youlogo = new Uploader('youtube_logo', 'logo');

        $web_logo = $weblogo->save();

        $twitter_logo = $twilogo->save();

        $instagram_logo = $inslogo->save();

        $telegram_logo = $tellogo->save();

        $youtube_logo = $youlogo->save();

        switch ($tabs) {
            case 'global':
                $options['global'] = [

                    'number_contact' => isset($_POST['phone_number_contact']) ? sanitize_text_field($_POST['phone_number_contact']) : '',
                    'twitter_address' => isset($_POST['twitter_address']) ? sanitize_text_field($_POST['twitter_address']) : '',
                    'instagram_address' => isset($_POST['instagram_address']) ? sanitize_text_field($_POST['instagram_address']) : '',
                    'youtube_address' => isset($_POST['youtube_address']) ? sanitize_text_field($_POST['youtube_address']) : '',
                    'telegram_address' => isset($_POST['telegram_address']) ? sanitize_text_field($_POST['telegram_address']) : '',

                    'show_number_contact' => isset($_POST['show_phone_number_contact']) ? 1 : 0,
                    'show_twitter' => isset($_POST['show_twitter']) ? 1 : 0,
                    'show_instagram' => isset($_POST['show_instagram']) ? 1 : 0,
                    'show_telegram' => isset($_POST['show_telegram']) ? 1 : 0,
                    'show_youtube' => isset($_POST['show_youtube']) ? 1 : 0,

                    'website_logo' => sanitize_text_field($web_logo) ? sanitize_text_field($web_logo) : Options::get_logo(Options::WEBSITE_LOGO, true),
                    'telegram_logo' => sanitize_text_field($telegram_logo) ? sanitize_text_field($telegram_logo) : Options::get_logo(Options::TELEGRAM_LOGO, true),
                    'instagram_logo' => sanitize_text_field($instagram_logo) ? sanitize_text_field($instagram_logo) : Options::get_logo(Options::INSTAGRAM_LOGO, true),
                    'twitter_logo' => sanitize_text_field($twitter_logo) ? sanitize_text_field($twitter_logo) : Options::get_logo(Options::TWITTER_LOGO, true),
                    'youtube_logo' => sanitize_text_field($youtube_logo) ? sanitize_text_field($youtube_logo) : Options::get_logo(Options::YOUTUBE_LOGO, true),

                ];

                break;
        }
        update_option(self::THEME_OPTION, $options);
    }
}
