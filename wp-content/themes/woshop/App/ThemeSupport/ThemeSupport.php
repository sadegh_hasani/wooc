<?php

namespace App\ThemeSupport;


class ThemeSupport
{
    public function supports()
    {
        add_theme_support('title-tag');
        add_theme_support('woocommerce');

        add_image_size('product-thumbnail', 250, 250, true);
        add_image_size('mini-logo', 42, 32, true);
    }

    public function register_mani_menu()
    {
        register_nav_menus([
            'main_menu' => 'main menu theme'
        ]);
    }

    public function register_footer_widget()
    {
        register_sidebar(array(
            'name' => 'ابزارک های فوتر',
            'id' => 'footer-1',
            'description' => 'ابزار های موجود در فوتر سایت',
            'before_widget' => '<div class="col-md-6">',
            'after_widget' => '</div>',
            'before_title' => '<h1>',
            'after_title' => '</h2>',
        ));
        register_sidebar(array(
            'name' => 'ابزارک های فوتر بخش 2',
            'id' => 'footer-2',
            'description' => 'ابزار های موجود در فوتر سایت',
            'before_widget' => '<ul class="widget_categories">',
            'after_widget' => '</ul>',
            'before_title' => '<h1>',
            'after_title' => '</h2>',
        ));
        register_sidebar(array(
            'name' => 'ابزارک فیلتر محصولات',
            'id' => 'filter-product',
            'description' => 'فیلتر محصولات ووکامرس',
            'before_widget' => '<div class="widget">',
            'after_widget' => '</div>',
            'before_title' => '<h1>',
            'after_title' => '</h2>',
        ));
    }
}
