<?php


namespace App\Services;


class Uploader
{
    private $file;

    private $upload_path;

    public function __construct($name, $sub_folder = null)
    {

        if ($name == []) {
            foreach ($name as $key) {
                $this->file = $_FILES[$key];
            }
        }

        $this->file = $_FILES[$name];

        if (!is_null($sub_folder) || !empty($sub_folder)) {
            if (!file_exists(UPLOAD_DIR . $sub_folder)) {
                @mkdir(UPLOAD_DIR . $sub_folder);
            }
        }

        if (is_null($sub_folder)) {
            $sub_folder = date("Y-m-h");
            if (!file_exists(UPLOAD_DIR . $sub_folder)) {
                @mkdir(UPLOAD_DIR . $sub_folder);
            }
        }
        $this->upload_path = UPLOAD_DIR . $sub_folder . DIRECTORY_SEPARATOR;
    }

    public function mime_type()
    {
        return $this->file['type'];
    }

    public function name()
    {
        return substr($this->file['name'], 0, 256);
    }

    public function extension()
    {
        $arr = explode('.', $this->name());
        return '.' . end($arr);
    }

    public function base_name()
    {
        return basename($this->name(), $this->extension());
    }

    private function white_list_mime_type()
    {
        $white_lists = ['.png', '.PNG', '.jpeg', '.JPEG', '.jpg', '.JPG'];

        return $white_lists;
    }

    //ToDo: implement this method for generate random name
    protected function generate_random_name()
    {
        $chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789_";
        $name = "";
        for ($i = 0; $i < 12; $i++)
            $name .= $chars[rand(0, strlen($chars))];
        return $name;
    }

    public function size()
    {
        return $this->file['size'];
    }

    private function upload($path)
    {
        return move_uploaded_file($this->file['tmp_name'], $path);
    }

    private function upload_file_name()
    {
        return $this->generate_random_name() . $this->extension();
    }

    public function save()
    {
        if (!in_array($this->extension(), array_keys($this->white_list_mime_type()))) {
            die('فایل مجاز آپلود نیست');
        }

        $file_name = $this->upload_file_name();
        $path = $this->upload_path . $file_name;


        if ($this->upload($path)) {
            return $file_name;
        } else {
            return false;
        }
    }
}