<?php

namespace App\Services;

class View
{
    public static function load($name, $data = [], $layout = null)
    {
        $file_name = str_replace('.', '/', $name) . '.php';
        $layout_name = str_replace('.', '/', $layout) . '.php';
        $file_path = THEME_PATH . "tpl/$file_name";
        if (file_exists($file_path) and is_readable($file_path)) {
            is_array($data) ? extract($data) : null;
            ob_start();
            include $file_path;
            $view = ob_get_clean();
            if ($layout == null) {
                echo $view;
                return;
            }
            $layout_path = THEME_PATH . 'tpl/layouts/' . "$layout_name";
            if (file_exists($layout_path)) {
                include $layout_path;
            } else {
                echo "file {$layout_path} not found";
            }
        } else {
            echo "file {$file_path} not found";
        }
    }
}


