<?php

namespace App\Widgets;

class Widget
{
    public function __construct()
    {
    }

    public function register_widgets()
    {
        register_sidebar([
            'name' => 'test',
            'id' => 'test',
            'description' => ' $description',
            'before_widget' => '$beforeWidget',
            'after_widget' => '$afterWidget',
            'before_title' => '$beforeTitle',
            'after_title' => '$afterTitle',
        ]);
    }
}