<?php get_header(); ?>
<?php get_template_part('tpl/front/header/header-wrapper') ?>
<div class="main_content">
    <div class="section">
        <div class="container">
            <?php the_content(); ?>
        </div>
    </div>
</div>
<?php get_footer() ?>