<!-- START FOOTER -->
<footer class="bg_gray">
    <div class="bottom_footer border-top-tran">
        <div class="container">
            <div class="row">

                <?php if (is_active_sidebar('footer-1')): ?>
                    <?php dynamic_sidebar('footer-1') ?>
                <?php endif; ?>

                <div class="col-lg-6">
                    <ul class="footer_payment text-center text-md-right">
                        <?php if (is_active_sidebar('footer-2')): ?>
                            <?php dynamic_sidebar('footer-2') ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER -->

<a href="#" class="scrollup" style="display: none;"><i class="ion-ios-arrow-up"></i></a>

<!-- Latest jQuery -->

<!-- popper min js -->

<?php wp_footer(); ?>
</body>

</html>