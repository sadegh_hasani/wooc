<?php

define('THEME_PATH', get_template_directory() . DIRECTORY_SEPARATOR);
define('THEME_URL', get_template_directory_uri() . '/');
$upload_path = wp_upload_dir();
define('UPLOAD_DIR', trailingslashit($upload_path['basedir'] . DIRECTORY_SEPARATOR));
define('UPLOAD_URL', trailingslashit($upload_path['baseurl']));
define('LOGO_URL', UPLOAD_URL . 'logo/');
