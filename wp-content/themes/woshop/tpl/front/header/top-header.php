<?php

use App\Utilities\Options;

?>
<div class="top-header light_skin bg_dark d-none d-md-block">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 col-md-8">
                <div class="header_topbar_info">
                    <div class="header_offer">
                        <span>ارسال رایگان خرید بیشتر از 100 هزار تومان</span>
                    </div>
                    <div class="download_wrap">
                        <span class="mr-3">دانلود برنامه</span>
                        <ul class="icon_list text-center text-lg-left">
                            <?php if (Options::is_show_logo(Options::SHOW_INSTAGRAM)) : ?>
                                <li>
                                    <a href="<?= Options::get_address(Options::INSTAGRAM_ADDRESS) ?>">
                                        <img src=" <?php echo Options::get_logo(Options::INSTAGRAM_LOGO) ?>" width=25
                                             height=25 alt="">
                                    </a>
                                </li>
                            <?php endif ?>

                            <?php if (Options::is_show_logo(Options::SHOW_TELEGRAM)) : ?>
                                <li>
                                    <a href="<?= Options::get_address(Options::TELEGRAM_ADDRESS) ?>">
                                        <img src=" <?php echo Options::get_logo(Options::TELEGRAM_LOGO) ?>" width=25
                                             height=25 alt="">
                                    </a>
                                </li>
                            <?php endif ?>

                            <?php if (Options::is_show_logo(Options::SHOW_YOUTUBE)) : ?>
                                <li>
                                    <a href="<?= Options::get_address(Options::YOUTUBE_ADDRESS) ?>">
                                        <img src=" <?php echo Options::get_logo(Options::YOUTUBE_LOGO) ?>" width=25
                                             height=25 alt="">
                                    </a>
                                </li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>