<?php


?>

<div class="bottom_header dark_skin main_menu_uppercase border-top">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-8 col-md-7 col-sm-6 col-7">
                <nav class="navbar navbar-expand-lg">
                    <button class="navbar-toggler side_navbar_toggler" type="button" data-toggle="collapse" data-target="#navbarSidetoggle" aria-expanded="false">
                        <span class="ion-android-menu"></span>
                    </button>
                    <div class="pr_search_icon">
                        <a href="javascript:void(0);" class="nav-link pr_search_trigger"><i class="linearicons-magnifier"></i></a>
                    </div>
                    <?php
                    if (has_nav_menu('main_menu')) : ?>
                        <?php wp_nav_menu([
                            'theme_location'  => 'main_menu',
                            'menu_class'      => 'navbar-nav',
                            'container'       => 'div',
                            'container_id'    => 'navbarSidetoggle',
                            'container_class' => 'collapse navbar-collapse mobile_side_menu',
                            'walker' => new App\Menu\ThemeMenu()
                        ]); ?>
                    <?php else : ?>
                        <div>
                            <h6 style="padding:5px;">برای این قسمت می توانید منویی قرار دهید</h6>
                        </div>
                    <?php endif ?>


                </nav>
            </div>
            <div class='col-lg-4 col-md-5 cold-sm-6 col-5'>
                <div class="contact_phone contact_support">
                    <i class="linearicons-phone-wave"></i>
                    <span>021-1234567</span>
                </div>
            </div>
        </div>
    </div>
</div>