<div class="middle-header dark_skin">
    <div class="container">
        <div class="nav_block">
            <a class="navbar-brand" href="<?php echo home_url() ?>">
                <img class="logo_light" src="<?= THEME_URL ?>assets/images/logo_light.png" alt="logo">
                <img class="logo_dark" src="<?= THEME_URL ?>assets/images/logo_dark.png" alt="logo">
            </a>
            <div class="product_search_form radius_input search_form_btn">
                <form role="search" method="get" class="woocommerce-product-search"
                      action="<?php echo esc_url(home_url('/')); ?>">
                    <div class="input-group">
                        <label class="screen-reader-text"
                               for="woocommerce-product-search-field-<?php echo isset($index) ? absint($index) : 0; ?>"><?php esc_html_e('Search for:', 'woocommerce'); ?></label>
                        <input type="search"
                               id="woocommerce-product-search-field-<?php echo isset($index) ? absint($index) : 0; ?>"
                               class="form-control"
                               placeholder="<?php echo esc_attr__('Search products&hellip;', 'woocommerce'); ?>"
                               value="<?php echo get_search_query(); ?>" name="s"/>
                        <button type="submit" class="search_btn3"
                                value="<?php echo esc_attr_x('Search', 'submit button', 'woocommerce'); ?>"><?php echo esc_html_x('Search', 'submit button', 'woocommerce'); ?></button>
                        <input type="hidden" name="post_type" value="product"/>
                    </div>
                </form>
            </div>
            <ul class="navbar-nav attr-nav align-items-center">
                <li><a href="<?php echo home_url('my-account') ?>" class="nav-link"><i class="linearicons-user"></i></a>
                </li>
                <li class="dropdown cart_dropdown">
                    <a class="nav-link cart_trigger" href="#" data-toggle="dropdown">
                        <i class="linearicons-bag2"></i>
                        <span class="cart_count"><?= WC()->cart->get_cart_contents_count() ?></span>
                        <span class="amount"><?php echo WC()->cart->get_cart_total() ?> </span>
                    </a>
                    <div class="cart_box cart_right dropdown-menu dropdown-menu-right">
                        <?php woocommerce_mini_cart(); ?>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>