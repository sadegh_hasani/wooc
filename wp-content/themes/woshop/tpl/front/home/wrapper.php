<!-- END MAIN CONTENT -->
<div class="main_content">

    <!-- START SECTION BANNER -->
    <?php get_template_part('tpl/front/home/top-banners') ?>
    <!-- END SECTION BANNER -->

    <!-- START SECTION CATEGORIES -->
    <?php get_template_part('tpl/front/home/fav-cat') ?>
    <!-- END SECTION CATEGORIES -->

    <!-- START SECTION SHOP -->
    <?php get_template_part('tpl/front/home/section-shop-1') ?>
    <!-- END SECTION SHOP -->

    <!-- START SECTION BANNER -->
    <?php get_template_part('tpl/front/home/stretch-banner') ?>
    <!-- END SECTION BANNER -->

    <!-- START SECTION SHOP -->
    <?php get_template_part('tpl/front/home/section-shop-2') ?>
    <!-- END SECTION SHOP -->

    <!-- START SECTION CLIENT LOGO -->
    <?php get_template_part('tpl/front/home/logo') ?>

    <!-- END SECTION CLIENT LOGO -->



</div>
<!-- END MAIN CONTENT -->