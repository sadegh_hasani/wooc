<div class="item">
    <div class="product_wrap">
        <a href="<?= get_the_permalink($prod->ID) ?>">
            <div class="product_img">
                <img src="<?= wp_get_attachment_image_url(get_post_thumbnail_id($prod->ID)) ?>" alt="el_img1">
            </div>
        </a>
        <div class="product_info">
            <h6 class="product_title"><a href="<?php echo get_the_permalink($prod->ID) ?>"><?php echo $prod->post_title ?></a></h6>
            <div class="product_price">
                <?php

                $_product_price = wc_get_product($prod->ID);
                $regular_price = $_product_price->get_regular_price();
                $sale_price = $_product_price->get_sale_price();
                if (isset($regular_price) && empty($sale_price)) {
                    echo "<span class='price'>" . number_format($regular_price) . " تومان </span>";
                }
                if (!empty($sale_price) && $regular_price > $sale_price) {
                    echo "<del>" . number_format($regular_price) . "</del>";
                    echo "<span class='price'>  تومان " . number_format($sale_price) . "</span>";
                }
                ?>
            </div>
        </div>
    </div>
</div>