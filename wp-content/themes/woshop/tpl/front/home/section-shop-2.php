<?php

use App\MetaBoxes\Product;

$popular_products = Product::get_meta_product(Product::POPULAR_PRODUCTS);


?>

<div class="section small_pt">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="heading_s1 text-center">
                    <h2>محصولات پربازدید</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="product_slider carousel_slider owl-carousel owl-theme dot_style1" data-loop="true" data-margin="20" data-responsive='{"0":{"items": "1"}, "481":{"items": "2"}, "768":{"items": "3"}, "991":{"items": "4"}}'>
                    <?php foreach ($popular_products as $pop_pro) : ?>
                        <div class="item">
                            <div class="product_wrap">
                                <a href="<?= get_the_permalink($pop_pro->ID) ?>">
                                    <div class="product_img">
                                        <img src="<?= wp_get_attachment_image_url(get_post_thumbnail_id($pop_pro->ID),'product-thumbnail') ?>" alt="el_img1">
                                    </div>
                                </a>
                                <div class="product_info">
                                    <h6 class="product_title"><a href="<?php echo get_the_permalink($pop_pro->ID) ?>"><?php echo $pop_pro->post_title ?></a></h6>
                                    <div class="product_price">
                                        <?php

                                        $_product_price = wc_get_product($pop_pro->ID);
                                        $regular_price = $_product_price->get_regular_price();
                                        $sale_price = $_product_price->get_sale_price();
                                        if (isset($regular_price) && empty($sale_price)) {
                                            echo "<span class='price'>" . number_format($regular_price) . " تومان </span>";
                                        }
                                        if (!empty($sale_price) && $regular_price > $sale_price) {
                                            echo "<del>" . number_format($regular_price) . "</del>";
                                            echo "<span class='price'>  تومان " . number_format($sale_price) . "</span>";
                                        }
                                        ?>
                                    </div>
                                </div>
                                <div class='add-to-card'>
                                    <a href="<?php the_permalink() ?>">خرید
                                        <span class="icon-basket-loaded"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>