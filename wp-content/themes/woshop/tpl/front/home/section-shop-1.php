<?php

use App\MetaBoxes\Product;

$new_products = Product::get_meta_product(Product::NEW_PRODUCT);
$sellers = Product::get_meta_product(Product::SELLER);
$special_products = Product::get_meta_product(Product::SPECIAL_PRODUCT);
$special_offers = Product::get_meta_product(Product::SPECIAL_OFFER);


?>

<div class="section small_pb small_pt">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="heading_s1 text-center">
                    <h2>محصولات اختصاصی</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="tab-style1">
                    <ul class="nav nav-tabs justify-content-center" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="arrival-tab" data-toggle="tab" href="#arrival" role="tab"
                               aria-controls="arrival" aria-selected="true">محصول جدید</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="sellers-tab" data-toggle="tab" href="#sellers" role="tab"
                               aria-controls="sellers" aria-selected="false">پرفروش</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="featured-tab" data-toggle="tab" href="#featured" role="tab"
                               aria-controls="featured" aria-selected="false">محصول ویژه</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="special-tab" data-toggle="tab" href="#special" role="tab"
                               aria-controls="special" aria-selected="false">پیشنهاد ویژه
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="tab_slider tab-content">
                    <div class="tab-pane fade show active" id="arrival" role="tabpanel" aria-labelledby="arrival-tab">
                        <div class="product_slider carousel_slider owl-carousel owl-theme dot_style1" data-loop="true"
                             data-margin="20"
                             data-responsive='{"0":{"items": "1"}, "481":{"items": "2"}, "768":{"items": "3"}, "991":{"items": "4"}}'>
                            <?php foreach ($new_products as $newp) : ?>
                                <div class="item">
                                    <div class="product_wrap">
                                        <a href="<?= get_the_permalink($newp->ID) ?>">
                                            <div class="product_img">
                                                <img src="<?= wp_get_attachment_image_url(get_post_thumbnail_id($newp->ID), 'product-thumbnail') ?>"
                                                     alt="el_img1">
                                            </div>
                                        </a>
                                        <div class="product_info">
                                            <h6 class="product_title"><a
                                                        href="<?php echo get_the_permalink($newp->ID) ?>"><?php echo $newp->post_title ?></a>
                                            </h6>
                                            <div class="product_price">
                                                <?php

                                                $_product_price = wc_get_product($newp->ID);
                                                $regular_price = $_product_price->get_regular_price();
                                                $sale_price = $_product_price->get_sale_price();
                                                if (isset($regular_price) && empty($sale_price)) {
                                                    echo "<span class='price'>" . number_format($regular_price) . " تومان </span>";
                                                }
                                                if (!empty($sale_price) && $regular_price > $sale_price) {
                                                    echo "<del>" . number_format($regular_price) . "</del>";
                                                    echo "<span class='price'>  تومان " . number_format($sale_price) . "</span>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class='add-to-card'>
                                            <a href="<?php the_permalink($newp->ID) ?>">خرید
                                                <span class="icon-basket-loaded"></span>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="sellers" role="tabpanel" aria-labelledby="sellers-tab">
                        <div class="product_slider carousel_slider owl-carousel owl-theme dot_style1" data-loop="true"
                             data-margin="20"
                             data-responsive='{"0":{"items": "1"}, "481":{"items": "2"}, "768":{"items": "3"}, "991":{"items": "4"}}'>

                            <?php foreach ($sellers as $sellerpro) : ?>
                                <div class="item">
                                    <div class="product_wrap">
                                        <a href="<?= get_the_permalink($sellerpro->ID) ?>">
                                            <div class="product_img">
                                                <img src="<?= wp_get_attachment_image_url(get_post_thumbnail_id($sellerpro->ID), 'product-thumbnail') ?>"
                                                     alt="el_img1">
                                            </div>
                                        </a>
                                        <div class="product_info">
                                            <h6 class="product_title"><a
                                                        href="<?php echo get_the_permalink($sellerpro->ID) ?>"><?php echo $sellerpro->post_title ?></a>
                                            </h6>
                                            <div class="product_price">
                                                <?php

                                                $_product_price = wc_get_product($sellerpro->ID);
                                                $regular_price = $_product_price->get_regular_price();
                                                $sale_price = $_product_price->get_sale_price();
                                                if (isset($regular_price) && empty($sale_price)) {
                                                    echo "<span class='price'>" . number_format($regular_price) . " تومان </span>";
                                                }
                                                if (!empty($sale_price) && $regular_price > $sale_price) {
                                                    echo "<del>" . number_format($regular_price) . "</del>";
                                                    echo "<span class='price'>  تومان " . number_format($sale_price) . "</span>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class='add-to-card'>
                                            <a href="<?php the_permalink() ?>">خرید
                                                <span class="icon-basket-loaded"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="featured" role="tabpanel" aria-labelledby="featured-tab">
                        <div class="product_slider carousel_slider owl-carousel owl-theme dot_style1" data-loop="true"
                             data-margin="20"
                             data-responsive='{"0":{"items": "1"}, "481":{"items": "2"}, "768":{"items": "3"}, "991":{"items": "4"}}'>
                            <?php foreach ($special_products as $special_product) : ?>
                                <div class="item">
                                    <div class="product_wrap">
                                        <a href="<?= get_the_permalink($special_product->ID) ?>">
                                            <div class="product_img">
                                                <img src="<?= wp_get_attachment_image_url(get_post_thumbnail_id($special_product->ID), 'product-thumbnail') ?>"
                                                     alt="el_img1">
                                            </div>
                                        </a>
                                        <div class="product_info">
                                            <h6 class="product_title"><a
                                                        href="<?php echo get_the_permalink($special_product->ID) ?>"><?php echo $special_product->post_title ?></a>
                                            </h6>
                                            <div class="product_price">
                                                <?php

                                                $_product_price = wc_get_product($special_product->ID);
                                                $regular_price = $_product_price->get_regular_price();
                                                $sale_price = $_product_price->get_sale_price();
                                                if (isset($regular_price) && empty($sale_price)) {
                                                    echo "<span class='price'>" . number_format($regular_price) . " تومان </span>";
                                                }
                                                if (!empty($sale_price) && $regular_price > $sale_price) {
                                                    echo "<del>" . number_format($regular_price) . "</del>";
                                                    echo "<span class='price'>  تومان " . number_format($sale_price) . "</span>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class='add-to-card'>
                                            <a href="<?php the_permalink() ?>">خرید
                                                <span class="icon-basket-loaded"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="special" role="tabpanel" aria-labelledby="special-tab">
                        <div class="product_slider carousel_slider owl-carousel owl-theme dot_style1" data-loop="true"
                             data-margin="20"
                             data-responsive='{"0":{"items": "1"}, "481":{"items": "2"}, "768":{"items": "3"}, "991":{"items": "4"}}'>
                            <?php foreach ($special_offers as $special_offer) : ?>
                                <div class="item">
                                    <div class="product_wrap">
                                        <a href="<?= get_the_permalink($special_offer->ID) ?>">
                                            <div class="product_img">
                                                <img src="<?= wp_get_attachment_image_url(get_post_thumbnail_id($special_offer->ID), 'product-thumbnail') ?>"
                                                     alt="el_img1">
                                            </div>
                                        </a>
                                        <div class="product_info">
                                            <h6 class="product_title"><a
                                                        href="<?php echo get_the_permalink($special_offer->ID) ?>"><?php echo $special_offer->post_title ?></a>
                                            </h6>
                                            <div class="product_price">
                                                <?php

                                                $_product_price = wc_get_product($special_offer->ID);
                                                $regular_price = $_product_price->get_regular_price();
                                                $sale_price = $_product_price->get_sale_price();
                                                if (isset($regular_price) && empty($sale_price)) {
                                                    echo "<span class='price'>" . number_format($regular_price) . " تومان </span>";
                                                }
                                                if (!empty($sale_price) && $regular_price > $sale_price) {
                                                    echo "<del>" . number_format($regular_price) . "</del>";
                                                    echo "<span class='price'>  تومان " . number_format($sale_price) . "</span>";
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class='add-to-card'>
                                            <a href="<?php the_permalink() ?>">خرید
                                                <span class="icon-basket-loaded"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>