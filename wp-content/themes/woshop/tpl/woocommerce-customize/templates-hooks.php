<?php


function before_product_title()
{
    ?>

    <a href="<?php the_permalink() ?>" style="padding-top: 10px; padding-bottom: 10px;">
        <?php the_post_thumbnail('product-thumbnail', ['class' => 'product_img_thumb']) ?>
    </a>
    <div class="product_info">
    <?php
}


function product_title()
{
    ?>
    <h6 class="product_title"><a href="<?php the_permalink() ?>"><?php the_title() ?></a></h6>
    <?php
}

function after_product_title()
{
    ?>
    <div class="product_price">
        <?php wc_get_template_part('loop/price') ?>
    </div>

    </div>
    <div class='add-to-card'>
        <a href="<?php the_permalink() ?>">خرید
            <span class="icon-basket-loaded"></span>
        </a>
    </div>
    <?php
}

/* --------------------------- custom_override_checkout_fields ------------------------*/


// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_billing_fields($fields)
{
    unset($fields['billing_company']);
    unset($fields['billing_country']);
    unset($fields['billing_address_2']);
    $fields['billing_address_1']['label'] = 'آدرس';

    return $fields;
}

function custom_override_checkout_shipping_fields($fields)
{
    unset($fields['shipping_company']);
    unset($fields['shipping_country']);
    unset($fields['shipping_address_2']);
    $fields['shipping_address_1']['label'] = 'آدرس';

    return $fields;
}



