<?php

remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20);

add_action('related_product', 'woocommerce_output_related_products');

/* ---------------------- before product title ------------------------ */
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10);
remove_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

add_action('woocommerce_before_shop_loop_item_title', 'before_product_title', 10);


/* ---------------------- product title ------------------------ */
remove_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10);
add_action('woocommerce_shop_loop_item_title', 'product_title', 10);


/* ---------------------- after product title ------------------------ */
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);

add_action('woocommerce_after_shop_loop_item_title', 'after_product_title', 10);


/* ----------------------- button add to card -------------------------- */
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);


/*  -----------------------------  unset checkout fields ----------------------------------- */

add_filter('woocommerce_billing_fields', 'custom_override_checkout_billing_fields');
add_filter('woocommerce_shipping_fields', 'custom_override_checkout_shipping_fields');


/*  ----------------------- archive  product hooks ---------------------*/

remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);

remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);

add_action('archive_breadcrumb', 'woocommerce_breadcrumb', 20);
