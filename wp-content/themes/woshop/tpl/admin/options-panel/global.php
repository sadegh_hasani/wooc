<?php

use App\Utilities\Options;

?>
<table class="form-table">
    <tbody>
    <tr align="top">
        <td scope="row">انتخاب لگوی سایت</td>
        <td>
            <input type="file" name="website_logo" multiple>
        </td>
    </tr>

    <?php if (Options::get_logo(Options::WEBSITE_LOGO)) : ?>
        <tr>
            <td>لگوی فعال</td>
            <td><img src="<?= Options::get_logo(Options::WEBSITE_LOGO); ?>" alt="" width="100" height="100"></td>
        </tr>
    <?php endif ?>
    <tr align="top">
        <td scope="row">شماره تماس</td>
        <td>
            <input type="text" name="phone_number_contact" value="<?= $options['number_contact'] ?? '' ?>">
        </td>
    </tr>
    <tr align="top">
        <td scope="row">نمایش شماره تماس (فعال)</td>
        <td>
            <input type="checkbox"
                   name="show_phone_number_contact" <?php checked(1, $options['show_number_contact']) ?>>
        </td>
    </tr>
</table>
<h3>شبکه های اجتمایی</h3>

<!-- Social Networks Logos  -->
<table class="form-table">
    <tbody>
    <tr align="top">
        <td>توییتر</td>
        <td>
            <input type="text" name="twitter_address" value="<?= $options['twitter_address'] ?? null ?>">
            <input type="file" name="twitter_logo">
        </td>
    </tr>
    ?>
    <?php if (Options::get_logo(Options::TWITTER_LOGO)) : ?>
        <tr>
            <td>لگوی فعال</td>
            <td><img src="<?= Options::get_logo(Options::TWITTER_LOGO); ?>" alt="" width="100" height="100"></td>
        </tr>
    <?php endif ?>
    <tr align="top">
        <td>اینستاگرام</td>
        <td>
            <input type="text" name="instagram_address" value="<?= $options['instagram_address'] ?? null ?>">
            <input type="file" name="instagram_logo">
        </td>
    </tr>
    <?php if (Options::get_logo(Options::INSTAGRAM_LOGO)) : ?>
        <tr>
            <td>لگوی فعال</td>
            <td><img src="<?= Options::get_logo(Options::INSTAGRAM_LOGO); ?>" alt="" width="100" height="100"></td>
        </tr>
    <?php endif ?>
    <tr align="top">
        <td>تلگرام</td>
        <td>
            <input type="text" name="telegram_address" value="<?= $options['telegram_address'] ?? null ?>">
            <input type="file" name="telegram_logo">
        </td>
    </tr>
    <?php if (Options::get_logo(Options::TELEGRAM_LOGO)) : ?>
        <tr>
            <td>لگوی فعال</td>
            <td><img src="<?= Options::get_logo(Options::TELEGRAM_LOGO); ?>" alt="" width="100" height="100"></td>
        </tr>
    <?php endif ?>
    <tr align="top">
        <td>یوتیوب</td>
        <td>
            <input type="text" name="youtube_address" value="<?= $options['youtube_address'] ?? null ?>">
            <input type="file" name="youtube_logo">
        </td>
    </tr>
    <?php if (Options::get_logo(Options::YOUTUBE_LOGO)) : ?>
        <tr>
            <td>لگوی فعال</td>
            <td><img src="<?= Options::get_logo(Options::YOUTUBE_LOGO); ?>" alt="" width="100" height="100"></td>
        </tr>
    <?php endif ?>
    <tr>
        <th>وضعیت نمایش</th>
        <td>
            <label for="">توییتر</label>
            <input type="checkbox" name="show_twitter" <?php checked(1, $options['show_twitter']) ?>>

            <label for="">اینستاگرام</label>
            <input type="checkbox" name="show_instagram" <?php checked(1, $options['show_instagram']) ?>>

            <label for="">تلگرام</label>
            <input type="checkbox" name="show_telegram" <?php checked(1, $options['show_telegram']) ?>>

            <label for="">یوتیوب</label>
            <input type="checkbox" name="show_youtube" <?php checked(1, $options['show_youtube']) ?>>
        </td>
    </tr>

    </tbody>
</table>

