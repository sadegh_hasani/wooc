<div class="notice-wrapper">
    <?php


    foreach ($messages as $message) : ?>

        <div class="message-<?= \app\Utilities\FlashMessage::get_css_type($message->type); ?>">
            <?php echo $message->msg ?>
        </div>
    <?php endforeach; ?>
</div>