<form action="" method="POST">

    <table class='form-table'>
        <?php foreach ($product_locations as $location => $name) : ?>
            <tr>
                <td>
                    <label for="<?= $name ?>"><?= $location ?></label>
                </td>
                <td>
                    <input type="checkbox" id="<?= $name ?>" name="product_location[]" value="<?= $name ?>" <?php echo in_array($name, $current_locations) ? 'checked' : '' ?>>
                </td>
            </tr>
        <?php endforeach ?>

    </table>

</form>