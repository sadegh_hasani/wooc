<div class='wrap'>
    <h1> ناحــیه ها</h1>
    <table class='table widefat'>
        <thead>
            <tr>
                <td>شناسه ناحیه </td>
                <td>نام ناحـیه</td>
                <td>عملیات</td>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($zones as $zone) : ?>
                <tr>
                    <td><?php echo $zone->zone_id ?></td>
                    <td><?php echo $zone->zone_name ?></td>
                    <td><a href="<?= add_query_arg(['action' => 'edit', 'id' => $zone->zone_id]) ?>"><span class='dashicons dashicons-edit'></span></a></td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>