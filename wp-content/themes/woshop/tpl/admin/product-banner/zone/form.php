<div class='wrap'>
    <h1>ویرایش ناحیه
        <a href="<?php echo add_query_arg(['action' => '']); ?>" class='page-title-action'>لیست ناحیه</a>
    </h1>
    <form action="" method="post">
        <table class='table form-table'>
            <tr>
                <td>نام ناحیه</td>
                <td><input type="text" name="zone_name" value="<?= isset($data['zone']->zone_name) ? $data['zone']->zone_name : '' ?>"></td>
            </tr>
        </table>

        <?php submit_button('دخیره ویرایش', 'primary', 'edit_zone') ?>

    </form>
</div>