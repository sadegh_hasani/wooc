<div class='wrap'>
    <h1>لیست بنر
        <a href="<?= add_query_arg(['action' => 'add_banner']) ?>" class='page-title-action'>افزودن بنر</a>
    </h1>

    <table class='table widefat'>
        <thead>
            <tr>
                <td>شناسه بنر </td>
                <td>عنوان بنر</td>
                <td>ناحیه بنر </td>
                <td>تصویر بنر</td>
                <td>آدرس بنر</td>
                <td>تاریخ ایجاد</td>
                <td>عملیات</td>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($banners as $banner) : ?>
              
                <tr>
                    <td><?= $banner->banner_id ?></td>
                    <td><?= $banner->banner_title ?></td>
                    <td><?= $banner->zone_name ?></td>
                    <td>
                        <img src="<?= BANNER_URL . $banner->banner_image  ?>" width="100" height="85">
                    </td>
                    <td><?= $banner->banner_link ?></td>
                    <td><?= $banner->created_at ?></td>
                    <td>
                        <a href="<?= add_query_arg(['action' => 'edit_banner', 'banner_id' => $banner->banner_id]) ?>"><i class='dashicons dashicons-edit'></i></a>
                        <a href="<?= add_query_arg(['action' => 'delete_banner', 'banner_id' => $banner->banner_id]) ?>"><i class='dashicons dashicons-trash'></i></a>
                    </td>
                </tr>
            <?php endforeach ?>
        </tbody>
    </table>
</div>