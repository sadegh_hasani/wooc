<div class="wrap">
    <h1>
        افزودن بنــر
        <a href="<?= add_query_arg(['action' => '']) ?>" class='page-title-action'>لیست بــنــرها</a>
    </h1>

    <form action="" method="post" enctype="multipart/form-data">
        <table class='form form-table'>
            <tr>
                <td>عنوان بنر:</td>
                <td>
                    <input type="text" name="banner_title" value="<?= isset($banner->banner_title) && !empty($banner->banner_title) ? $banner->banner_title : "" ?>">
                </td>
            </tr>

            <tr>
                <td>جایگاه بنر:</td>
                <td>
                    <select name="banner_zone" id="banner_zone">
                        <option value="">-- انتخاب جایگاه --</option>
                        <?php foreach ($zones as $zone) : ?>
                            <option value="<?= $zone->zone_id ?>" <?php isset($banner->zone_id) && !empty($banner->zone_id) ?  selected($zone->zone_id, $banner->zone_id) : "" ?>>
                                -- <?= $zone->zone_name ?> --
                            </option>
                        <?php endforeach; ?>
                    </select>
                </td>
            </tr>

            <tr>
                <td>تصویر بنر:</td>
                <td>
                    <input type="file" name="banner_image" value="<?= $banner->banner_image ?>">
                </td>
            </tr>

            <tr>
                <td>آدرس بنر:</td>
                <td>
                    <input type="text" name="banner_link" value="<?php echo isset($banner->banner_link) && !empty($banner->banner_link) ? $banner->banner_link : "" ?>">
                </td>
            </tr>

        </table>

        <?php submit_button('ساخت بنر', 'primary', 'c_banner') ?>
    </form>
</div>