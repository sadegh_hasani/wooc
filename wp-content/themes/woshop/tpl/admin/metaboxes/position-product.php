<div class='product-position-wrapper'>
    <?php foreach ($position_list as $item => $key) : ?>
        <div class='product-position-inner'>
            <label for="<?= $key ?>"><?= $item ?></label>
            <input type="checkbox" id="<?= $key ?>" name="athome_product_position[]" value="<?php echo $key ?>" <?php echo in_array($key, $product_position) ? 'checked' : '' ?>>
        </div>
        <hr>
    <?php endforeach ?>
</div>