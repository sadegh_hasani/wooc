<div class="wrap">
    <h2>تنطیمات قالب</h2>

    <nav class="nav-tab-wrapper">
        <a href="<?= add_query_arg(['tab' => 'global']) ?>" class="nav-tab <?= ($data['tabs'] == 'global') ? 'nav-tab-active' : '' ?>">عمومی</a>
        <!-- <a href="<?//= add_query_arg(['tab' => 'post']) ?>" class="nav-tab <?//= ($data['tabs'] == 'post') ? 'nav-tab-active' : '' ?>">پست</a> -->
    </nav>

    <div class="content-wrapper">
        <form action="" method="POST" enctype="multipart/form-data">
            <?php echo $view; ?>
            <?php submit_button('ذخیـره تـغـیـیـرات', 'primary', 'otpion_theme_save'); ?>
        </form>
    </div>
</div>