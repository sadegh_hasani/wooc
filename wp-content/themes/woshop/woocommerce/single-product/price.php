<?php

/**
 * Single Product Price
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/price.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.0.0
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly
}

global $product;

$regular_price = $product->get_regular_price();
$sale_price = $product->get_sale_price();


?>

<p class="<?php echo esc_attr(apply_filters('woocommerce_product_price_class', 'price')); ?>">
	<?php
	if (isset($regular_price) && empty($sale_price)) {
		echo "<span class='price'>" . number_format($regular_price) . " تومان </span>";
	}
	if (!empty($sale_price) && $regular_price > $sale_price) {
		echo "<del style='color:#808080bf'>" . number_format($regular_price) . "</del>";
		echo "<span class='price'> " . number_format($sale_price) . "   تومان </span>";
	}
	?>
</p>