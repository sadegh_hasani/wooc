<?php

/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.1
 */

defined('ABSPATH') || exit;

// Note: `wc_get_gallery_image_html` was added in WC 3.3.2 and did not exist prior. This check protects against theme overrides being used on older versions of WC.
if (!function_exists('wc_get_gallery_image_html')) {
	return;
}

global $product;

$post_thumbnail_id = $product->get_image_id();
$attachment_ids = $product->get_gallery_image_ids();
?>

<div class="product_img_box">
	<img id="product_img" src="<?php echo wp_get_attachment_image_url($post_thumbnail_id, 'single-woocommerce') ?>" data-zoom-image="<?php echo wp_get_attachment_image_url($post_thumbnail_id, 'single-woocommerce') ?>" alt="product_img1">
	<a href="#" class="product_img_zoom" title="Zoom">
		<span class="linearicons-zoom-in"></span>
	</a>
</div>


<div id="pr_item_gallery" class="product_gallery_item slick_slider" data-slides-to-show="4" data-slides-to-scroll="1" data-infinite="false">
	<?php foreach ($attachment_ids as $img) : ?>
		<div class="item">
			<a href="#" class="product_gallery_item" data-image="<?= wp_get_attachment_image_url($img, 'single-woocommerce') ?>" data-zoom-image="<?= wp_get_attachment_image_url($img, 'single-woocommerce') ?>" tabindex="0">
				<img src="<?= wp_get_attachment_image_url($img) ?>" alt="product_small_img4">
			</a>
		</div>
	<?php endforeach; ?>
</div>

<!-- 

<div id="pr_item_gallery" class="product_gallery_item slick_slider" data-slides-to-show="4" data-slides-to-scroll="1" data-infinite="false">
	<div class="item">
		<a href="#" class="product_gallery_item active" data-image="<?//= THEME_URL ?>assets/images/product_img1.jpg" data-zoom-image="<?//= THEME_URL ?>assets/images/product_zoom_img1.jpg">
			<img src="<?//= THEME_URL ?>assets/images/product_small_img1.jpg" alt="product_small_img1" />
		</a>
	</div>
</div> -->