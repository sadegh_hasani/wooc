jQuery(document).ready(function () {
    var $ = jQuery;
    if ($('.woshop_add_image').length > 0) {
        if (typeof wp !== 'undefined' && wp.media && wp.media.editor) {
            $('.woshop_add_image').on('click', function (e) {
                e.preventDefault();
                var button = $(this);
                var id = button.prev();
                wp.media.editor.send.attachment = function (props, attachment) {
                    id.val(attachment.id);
                };
                wp.media.editor.open(button);
                return false;
            });
        }
    }
});