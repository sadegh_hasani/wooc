<?php

use App\MetaBoxes\Product;
use App\ThemeSupport\ThemeSupport;

require "bootstrap/constants.php";
require "vendor/autoload.php";

require "tpl/woocommerce-customize/hooks.php";
require "tpl/woocommerce-customize/templates-hooks.php";
require "App/Menu/ThemeMenu.php";

add_filter('show_admin_bar', '__return_false');


/* after setup theme */
$theme_support = new ThemeSupport();
add_action('after_setup_theme', [$theme_support, 'supports']);
add_action('after_setup_theme', [$theme_support, 'register_mani_menu']);
add_action('widgets_init', [$theme_support, 'register_footer_widget']);
/* metaboxes product */


$product_metabox = new Product();
add_action('add_meta_boxes', [$product_metabox, 'register_product_metabox']);
add_action('save_post', [$product_metabox, 'save_product_location']);

/* register assets */
$asset = new \App\Asset\Asset();
add_action('wp_enqueue_scripts', [$asset, 'register_assets_theme']);

/* add media button */
add_action('admin_enqueue_scripts', function () {
    if (is_admin()) {
        wp_enqueue_media();
    }
});

/* widget`s theme */


/* theme option panel */
$options_panel = new \App\OptionsPanel\optionsPanel();
add_action('admin_menu', [$options_panel, 'register_options_panel_menu']);

