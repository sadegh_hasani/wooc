# Translation of Plugins - YITH WooCommerce Ajax Product Filter - Stable (latest release) in Persian
# This file is distributed under the same license as the Plugins - YITH WooCommerce Ajax Product Filter - Stable (latest release) package.
msgid ""
msgstr ""
"PO-Revision-Date: 2020-12-07 13:58:27+0000\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: GlotPress/3.0.0-alpha.2\n"
"Language: fa\n"
"Project-Id-Version: Plugins - YITH WooCommerce Ajax Product Filter - Stable (latest release)\n"

#: plugin-fw/templates/sysinfo/tabs/main.php:20
msgid "Site URL"
msgstr "نشانی اینترنتی سایت"

#: plugin-fw/includes/class-yith-system-status.php:342
msgid "Warning!"
msgstr "هشدار!"

#: plugin-fw/includes/class-yith-system-status.php:191
msgid "WooCommerce"
msgstr "ووکامرس"

#: plugin-fw/includes/class-yith-system-status.php:122
msgid "URL FOpen"
msgstr "نشانی اینترنتی FOpen"

#: plugin-fw/includes/class-yith-system-status.php:121
msgid "OPCache Save Comments"
msgstr "OPCache نظرات را ذخیره کنید"

#: plugin-fw/includes/class-yith-system-status.php:120
msgid "Iconv Module"
msgstr "ماژول Iconv"

#: plugin-fw/includes/class-yith-system-status.php:119
msgid "GD Library"
msgstr "کتابخانه GD"

#: plugin-fw/includes/class-yith-system-status.php:118
msgid "ImageMagick Version"
msgstr "نگارش ImageMagick"

#: plugin-fw/includes/class-yith-system-status.php:573
msgid "Contact your hosting company in order to enable it."
msgstr "برای فعال کردن آن با شرکت میزبان خود تماس بگیرید."

#: plugin-fw/includes/class-yith-system-status.php:554
#: plugin-fw/includes/class-yith-system-status.php:560
msgid "Contact your hosting company in order to update it."
msgstr "جهت بروزرسانی آن با شرکت میزبان خود تماس بگیرید."

#: plugin-fw/includes/class-yith-system-status.php:488
msgid "Disabled"
msgstr "غیرفعال شده"

#: plugin-fw/includes/class-yith-system-status.php:488
msgid "Enabled"
msgstr "فعال شده"

#. translators: %1$s opening link tag, %2$s closing link tag
#: plugin-fw/includes/class-yith-system-status.php:577
#: plugin-fw/includes/class-yith-system-status.php:600
msgid "Read more %1$shere%2$s or contact your hosting company in order to increase it."
msgstr "بیشتر بخوانید %1$s اینجا%2$s یا با میزبان خود برای افزایش آن تماس بگیرید."

#. translators: %1$s code, %2$s file name
#: plugin-fw/includes/class-yith-system-status.php:565
msgid "Remove %1$s from %2$s file"
msgstr "پاک کردن پرونده %1$s از %2$s"

#: plugin-fw/includes/builders/gutenberg/class-yith-gutenberg.php:147
msgctxt "[gutenberg]: Category Name"
msgid "YITH"
msgstr "YITH"

#: plugin-fw/yit-plugin.php:203
msgid "License"
msgstr "لایسنس"

#: plugin-fw/yit-plugin.php:199
msgctxt "Action links"
msgid "Settings"
msgstr "تنظیمات"

#: plugin-fw/yit-plugin.php:96
msgctxt "Plugin Row Meta"
msgid "Premium version"
msgstr "نگارش پولی"

#: plugin-fw/yit-plugin.php:84
msgctxt "Plugin Row Meta"
msgid "Live Demo"
msgstr "دموی زنده"

#: plugin-fw/includes/privacy/class-yith-privacy.php:106
msgctxt "Privacy Policy Content"
msgid "Payments"
msgstr "پرداخت‌ها"

#: plugin-fw/includes/privacy/class-yith-privacy.php:61
msgctxt "Privacy Policy Guide Title"
msgid "YITH Plugins"
msgstr "افزونه های YITH"

#: plugin-fw/templates/fields/icons.php:63
msgid "Set Default"
msgstr "تنظیم پیشفرض"

#: plugin-fw/templates/fields/ajax-products.php:14
msgid "Search Product"
msgstr "جستجوی محصول"

#: plugin-fw/templates/fields/ajax-posts.php:41
msgid "Search Posts"
msgstr "جستجوی نوشته‌ها"

#: includes/widgets/class.yith-wcan-navigation-widget.php:47
msgid "Filter the list of products without reloading the page"
msgstr "فیلتر کردن لیست محصولات بدون بارگذاری مجدد برگه"

#: plugin-fw/includes/class-yit-plugin-panel.php:451
#: plugin-fw/includes/class-yit-plugin-panel.php:454
msgid "How to install premium version"
msgstr "چگونه نسخه پولی را نصب کنید"

#: includes/widgets/class.yith-wcan-navigation-widget.php:486
msgid "CSS custom class"
msgstr "کلاس سفارشی css"

#: plugin-fw/templates/fields/upload.php:39
msgid "Reset"
msgstr "بازنشانی"

#: init.php:145
msgid "You can't activate the free version of YITH WooCommerce Ajax Product Filter while you are using the premium one."
msgstr "شما نمی توانید نسخه رایگان این افزونه را فعال کنید تا زمانی که در حال استفاده از نسخه پولی افزونه هستید."

#: includes/widgets/class.yith-wcan-navigation-widget.php:443
#: includes/widgets/class.yith-wcan-reset-navigation-widget.php:194
msgid "Title"
msgstr "عنوان"

#: plugin-fw/templates/fields/image-gallery.php:41
msgid "Delete"
msgstr "حذف"

#: includes/widgets/class.yith-wcan-navigation-widget.php:479
msgid "Only Parent"
msgstr "فقط والد"

#: includes/widgets/class.yith-wcan-navigation-widget.php:468
msgid "Attribute:"
msgstr "ویژگی:"

#: includes/widgets/class.yith-wcan-navigation-widget.php:463
msgid "OR"
msgstr "یا"

#: includes/widgets/class.yith-wcan-navigation-widget.php:460
msgid "Query Type:"
msgstr "نوع کوئری:"

#: includes/widgets/class.yith-wcan-navigation-widget.php:449
msgid "Type:"
msgstr "نوع:"

#: includes/widgets/class.yith-wcan-navigation-widget.php:436
msgid "Dropdown"
msgstr "کرکره ای"

#: includes/widgets/class.yith-wcan-navigation-widget.php:435
msgid "Label"
msgstr "برچسب"

#: includes/widgets/class.yith-wcan-navigation-widget.php:433
msgid "List"
msgstr "لیست"

#: includes/widgets/class.yith-wcan-navigation-widget.php:201
msgid "Filters:"
msgstr "فیلترها:"

#: plugin-fw/templates/panel/woocommerce/woocommerce-form.php:38
msgid "Reset Defaults"
msgstr "بازنشانی پیشفرض‌ها"

#: plugin-fw/templates/fields/image-gallery.php:38
msgid "Add to gallery"
msgstr "افزودن به گالری"

#: plugin-fw/templates/fields/image-gallery.php:39
msgid "Add images"
msgstr "افزودن تصاویر"

#: plugin-fw/templates/fields/sidebars.php:20
msgid "Left sidebar"
msgstr "ناحیه کناری چپ"

#: plugin-fw/templates/fields/sidebars.php:26
#: plugin-fw/templates/fields/sidebars.php:29
msgid "No sidebar"
msgstr "بدون ناحیه کناری"

#: plugin-fw/templates/fields/sidebars.php:23
msgid "Right sidebar"
msgstr "ناحیه کناری راست"

#: plugin-fw/templates/fields/sidebars.php:41
#: plugin-fw/templates/fields/sidebars.php:56
msgid "Choose a sidebar"
msgstr "انتخاب ناحیه کناری"

#: plugin-fw/templates/fields/sidebars.php:39
msgid "Left Sidebar"
msgstr "ناحیه کناری چپ"

#: plugin-fw/templates/fields/sidebars.php:54
msgid "Right Sidebar"
msgstr "ناحیه کناری راست"

#: includes/widgets/class.yith-wcan-navigation-widget.php:475
msgid "Display (default All):"
msgstr "نمایش (پیشفرض همه):"

#: includes/widgets/class.yith-wcan-navigation-widget.php:477
msgid "All (no hierarchical)"
msgstr "همه (بدون سلسله مراتب)"

#: includes/widgets/class.yith-wcan-navigation-widget.php:478
msgid "All (hierarchical)"
msgstr "همه (سلسله مراتبی)"

#: includes/widgets/class.yith-wcan-reset-navigation-widget.php:187
msgid "Reset All Filters"
msgstr "بازنشانی همه فیلترها"

#: includes/widgets/class.yith-wcan-reset-navigation-widget.php:200
msgid "Button Label"
msgstr "نام دکمه"

#. Plugin Name of the plugin
msgid "YITH WooCommerce Ajax Product Filter"
msgstr "افزونه فیلتر محصول ووکامرس"

#. Author URI of the plugin
msgid "https://yithemes.com/"
msgstr "http://yithemes.com/"

#: plugin-fw/includes/class-yit-plugin-panel.php:84
msgid "Settings"
msgstr "تنظیمات"

#: plugin-fw/includes/class-yit-plugin-subpanel.php:132
#: plugin-fw/includes/class-yit-plugin-panel.php:620
msgid "Save Changes"
msgstr "ذخیره تغییرات"

#: plugin-fw/templates/fields/customtabs.php:25
#: plugin-fw/templates/fields/customtabs.php:68
msgid "Remove"
msgstr "حذف"

#: plugin-fw/templates/fields/customtabs.php:34
#: plugin-fw/templates/fields/customtabs.php:76
msgid "Name"
msgstr "نام"

#: includes/functions.yith-wcan.php:98 includes/functions.yith-wcan.php:112
#: includes/functions.yith-wcan.php:132
msgid "Term"
msgstr "شرایط."

#: includes/widgets/class.yith-wcan-navigation-widget.php:434
#: includes/functions.yith-wcan.php:98
msgid "Color"
msgstr "رنگ"

#: includes/functions.yith-wcan.php:112
msgctxt "For multicolor: I.E. white and red T-Shirt"
msgid "Color 1"
msgstr "رنگ 1"

#: includes/functions.yith-wcan.php:112
msgctxt "For multicolor: I.E. white and red T-Shirt"
msgid "Color 2"
msgstr "رنگ 2"

#: includes/functions.yith-wcan.php:132
msgid "Labels"
msgstr "برچسب‌ها"

#: init.php:163
msgid "YITH WooCommerce Ajax Product Filter is enabled but not effective. It requires WooCommerce in order to work."
msgstr "افزونه فیلتر محصول ووکامرس فعال است اما تاثیری ندارد. باید ووکامرس را نصب کنید."

#: plugin-fw/templates/fields/ajax-terms.php:41
msgid "Search Categories"
msgstr "جستجوی دسته‌ها"

#: plugin-fw/includes/class-yit-plugin-panel-woocommerce.php:419
msgid "The changes you have made will be lost if you leave this page."
msgstr "تغییراتی که در این صفحه ایجاد کرده اید با ترک آن ازبین می رود."

#: plugin-fw/includes/class-yit-plugin-panel.php:83
msgid "Plugin Settings"
msgstr "تنظیمات افزونه"

#: plugin-fw/templates/panel/woocommerce/woocommerce-form.php:14
#: plugin-fw/includes/class-yit-plugin-subpanel.php:136
#: plugin-fw/includes/class-yit-plugin-panel.php:625
msgid "If you continue with this action, you will reset all options in this page."
msgstr "اگر این عمل را ادامه دهید، تمام تنظیمات این صفحه بازگردانی می شود."

#: plugin-fw/includes/class-yit-plugin-subpanel.php:139
#: plugin-fw/includes/class-yit-plugin-panel.php:629
msgid "Reset to default"
msgstr "بازگشت به پیشفرض"

#: plugin-fw/templates/panel/woocommerce/woocommerce-form.php:14
#: plugin-fw/includes/class-yit-plugin-subpanel.php:136
#: plugin-fw/includes/class-yit-plugin-panel.php:625
msgid "Are you sure?"
msgstr "آیا اطمینان دارید?"

#: plugin-fw/includes/class-yit-plugin-panel.php:868
msgid "The element you have entered already exists. Please, enter another name."
msgstr "عنصری که وارد کردید وجود دارد. نام دیگری وارد کنید."

#: plugin-fw/includes/class-yit-plugin-panel.php:869
msgid "Settings saved"
msgstr "تنظیمات ذخیره شد"

#: plugin-fw/includes/class-yit-plugin-panel.php:870
msgid "Settings reset"
msgstr "تنظیمات بازگردانی شد"

#: plugin-fw/includes/class-yit-plugin-panel.php:871
msgid "Element deleted correctly."
msgstr "عنصر به درستی حذف شد."

#: plugin-fw/includes/class-yit-plugin-panel.php:872
#: plugin-fw/includes/class-yit-plugin-panel.php:873
msgid "Element updated correctly."
msgstr "عنصر به درستی بروزرسانی شد."

#: plugin-fw/includes/class-yit-plugin-panel.php:874
msgid "Database imported correctly."
msgstr "پایگاه داده به درستی درون‌ریزی شد."

#: plugin-fw/includes/class-yit-plugin-panel.php:875
msgid "An error has occurred during import. Please try again."
msgstr "خطائی هنگام درون‌ریزی رخ داده است. مجدد تلاش کنید."

#: plugin-fw/includes/class-yit-plugin-panel.php:876
msgid "The added file is not valid."
msgstr "پرونده افزوده شده معتبر نیست."

#: plugin-fw/includes/class-yit-plugin-panel.php:877
msgid "Sorry, import is disabled."
msgstr "متاسفانه، درون‌ریزی غیرفعال است."

#: plugin-fw/includes/class-yit-plugin-panel.php:878
msgid "Sorting successful."
msgstr "مرتب‌سازی موفق بود."

#: plugin-fw/includes/class-yit-pointers.php:80
msgid "Plugins Activated"
msgstr "افزونه‌ها فعال شدند"

#: plugin-fw/includes/class-yit-pointers.php:96
msgid "Plugins Upgraded"
msgstr "افزونه‌ها ارتقاء یافتند"

#: plugin-fw/templates/fields/upload.php:34
#: plugin-fw/templates/panel/woocommerce/woocommerce-upload.php:37
msgid "Upload"
msgstr "بارگذاری"

#: plugin-fw/templates/fields/customtabs.php:17
msgid "Close all"
msgstr "بستن همه"

#: plugin-fw/templates/fields/customtabs.php:17
msgid "Expand all"
msgstr "گسترش همه"

#: plugin-fw/templates/fields/customtabs.php:40
#: plugin-fw/templates/fields/customtabs.php:81
msgid "Value"
msgstr "مقدار"

#: plugin-fw/templates/fields/customtabs.php:41
#: plugin-fw/templates/fields/customtabs.php:82
msgid "Content of the tab. (HTML is supported)"
msgstr "محتوای تب.( HTML پیشتیبانی می شود)"

#: plugin-fw/templates/fields/customtabs.php:52
msgid "Add custom product tab"
msgstr "افزودن تب سفارشی محصول"

#: plugin-fw/templates/fields/customtabs.php:93
msgid "Do you want to remove the custom tab?"
msgstr "آیا میخواهید تب سفارشی را حذف کنید?"

#: plugin-fw/templates/fields/image-gallery.php:29
#: plugin-fw/templates/fields/image-gallery.php:40
msgid "Delete image"
msgstr "حذف تصویر"

#: plugin-fw/templates/fields/image-gallery.php:37
msgid "Add Images to Gallery"
msgstr "افزودن تصاویر به گالری"

#: plugin-fw/includes/class-yith-system-status.php:183
msgid "YITH Plugins"
msgstr "افزونه های YITH"